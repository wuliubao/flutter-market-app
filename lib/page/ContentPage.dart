import 'package:flutter/material.dart';

import 'package:flutteritem/component/confirm.dart';
import 'package:flutteritem/component/StaticTabBarSelector.dart';
import 'package:flutteritem/component/EdiabletList.dart';
import 'dart:ui' as ui;

class ContentPage extends StatefulWidget {
  ContentPage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _ContentPageState createState() => new _ContentPageState();
}

//item array
final List<DataItem> dataItemList = <DataItem>[
  new DataItem(
    title: 'The Old Man and The Sea',
    url: 'images/1.png',
    images: [
      'images/1.png',
      'images/1.png',
      'images/1.png',
      'images/1.png',
      'images/1.png'
    ],
    introduction: 'He was an old man who fished alone in a shiff in the GulfStream',
    content: 'He was an old man who fished alone in a shiff in the GulfStream He was an old man who fished alone in a shiff in the GulfStream',
  ),
  new DataItem(
    title: 'The Godfather',
    url: 'images/2.png',
    images: [
      'images/2.png',
      'images/2.png',
      'images/2.png',
      'images/2.png',
      'images/2.png'
    ],
    introduction: 'Never tell anybody outside the family what you are thinking again',
    content: 'Never tell anybody outside the family what you are thinking again Never tell anybody outside the family what you are thinking again',
  ),
  new DataItem(
    title: 'Walden',
    url: 'images/3.png',
    images: [
      'images/3.png',
      'images/3.png',
      'images/3.png',
      'images/3.png',
      'images/3.png'
    ],
    introduction: 'Most men, even in this comparatively free country, through,',
    content: 'Most men, even in this comparatively free country, through,Most men, even in this comparatively free country, through,',
  ),
  new DataItem(
    title: 'Modern Day',
    url: 'images/4.png',
    images: [
      'images/4.png',
      'images/4.png',
      'images/4.png',
      'images/4.png',
      'images/4.png'
    ],
    introduction: 'Many people around the world take me',
    content: 'Many people around the world take me Many people around the world take me Many people around the world take me',
  ),
  new DataItem(
    title: 'The Old Man and The Sea',
    url: 'images/1.png',
    images: [
      'images/1.png',
      'images/1.png',
      'images/1.png',
      'images/1.png',
      'images/1.png'
    ],
    introduction: 'He was an old man who fished alone in a shiff in the GulfStream',
    content: 'He was an old man who fished alone in a shiff in the GulfStream He was an old man who fished alone in a shiff in the GulfStream',
  ),
  new DataItem(
    title: 'The Godfather',
    url: 'images/2.png',
    images: [
      'images/2.png',
      'images/2.png',
      'images/2.png',
      'images/2.png',
      'images/2.png'
    ],
    introduction: 'Never tell anybody outside the family what you are thinking again',
    content: 'Never tell anybody outside the family what you are thinking again Never tell anybody outside the family what you are thinking again',
  ),
  new DataItem(
    title: 'Walden',
    url: 'images/3.png',
    images: [
      'images/3.png',
      'images/3.png',
      'images/3.png',
      'images/3.png',
      'images/3.png'
    ],
    introduction: 'Most men, even in this comparatively free country, through,',
    content: 'Most men, even in this comparatively free country, through, Most men, even in this comparatively free country, through,',
  ),
  new DataItem(
    title: 'Modern Day',
    url: 'images/4.png',
    images: [
      'images/4.png',
      'images/4.png',
      'images/4.png',
      'images/4.png',
      'images/4.png'
    ],
    introduction: 'Many people around the world take me',
    content: 'Many people around the world take me Many people around the world take me Many people around the world take me',
  ),
];


//data struct
class DataItem {
  DataItem({
    this.title,
    this.url,
    this.introduction,
    this.content,
    this.images,
  });

  final String title;
  final String url;
  final String introduction;
  final String content;
  final List<String> images;
  double height = 100.0;

  bool showDetail = false;
  double normalHeight = 140.0;
  double detailHeight = 280.0;
}

class OperateDataList<E> {
  OperateDataList({
    @required this.listKey,
    @required this.removedItemBuilder,
    Iterable<E> initialItems,
  })
      : assert(listKey != null),
        assert(removedItemBuilder != null),
        _items = List<E>.from(initialItems??<E>[]);

  final GlobalKey<AnimatedListState> listKey;
  final dynamic removedItemBuilder;
  final List<E> _items;

  AnimatedListState get _animatedList => listKey.currentState;

  void insert(int index, E item) {
    _items.insert(index, item);
    _animatedList.insertItem(index);
  }

  E removeAt(int index) {
    final E removedItem = _items.removeAt(index);
    if (removedItem != null) {
      _animatedList.removeItem(
          index, (BuildContext context, Animation<double> animation) {
        return removedItemBuilder(removedItem, context, animation);
      });
    }
    return removedItem;
  }

  int get length => _items.length;

  E operator [](int index) => _items[index];

  int indexOf(E item) => _items.indexOf(item);
}

class _ContentPageState extends State<ContentPage>
    with TickerProviderStateMixin {

  bool show = true;
  bool editState = false;

  ScrollController _scrollController = new ScrollController();
  TabController _tabController;

  Animation<double> animation;
  AnimationController controller;

  Offset start;

  double itemHeight = 140.0;

  @override
  void initState() {
    _scrollController.addListener(() {
      print("scroll?");
    });
    super.initState();

    operateDataList = new OperateDataList<DataItem>(
      listKey: operateKey,
      initialItems: dataItemList,
      removedItemBuilder: buildRemovedWidgetItem,
    );

    newDataItem = new DataItem(
      title: 'Modern Day',
      url: 'images/4.png',
      images: [
        'images/4.png',
        'images/4.png',
        'images/4.png',
        'images/4.png',
        'images/4.png'
      ],
      content: 'Many people around the world take me',
    );
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  void itemTapCallback() {
  }

  void startDrag() {
    setState(() {
      //show = false;
    });
  }

  void startConfirm(TapUpDetails details) {
    print("touch start?");
//    start = (context.findRenderObject() as RenderBox)
//        .globalToLocal(details.globalPosition);

    setState(() {
      start = details.globalPosition;
      show = false;
      controller = AnimationController(
          vsync: this, duration: Duration(milliseconds: 1500));
      final CurvedAnimation curve = new CurvedAnimation(
          parent: controller, curve: Curves.linear);
      animation = new Tween(begin: 0.0, end: 2.0).animate(curve)
        ..addListener(() {
          setState(() {
            // the state that has changed here is the animation object’s value
          });
        })
        ..addStatusListener((status) {
          if (status == AnimationStatus.forward) {
            print("动画开始");
          } else if (status == AnimationStatus.completed) {
            print("动画结束");
            show = true;
            controller.stop();
          }
          else if (status == AnimationStatus.dismissed) {
            print("动画消失");
            //   show = true;
            // controller.forward();
          }
        });
      controller.forward(from: 0.0);
    });
  }

  Widget buildListItemInner(String item) {
    return new Container(
      decoration: new BoxDecoration(
        border: Border.all(color: Colors.white),
        color: Colors.white,
        borderRadius: BorderRadius.circular(5.0),
        boxShadow: const <BoxShadow>[
          BoxShadow(
            offset: Offset(0.0, 3.0),
            blurRadius: 2.0,
            spreadRadius: -1.0,
            color: Color(0x33000000),
          ),
        ],
      ),
      child: new Image.asset(item),
    );
  }

  Widget buildListItem(DataItem item) {
    return new GestureDetector(
        onTap: () {
          setState(() {
            if (editState == false) {
              item.showDetail = !item.showDetail;
            } else {
            //  selectedDataItem = selectedDataItem == item ? null : item;
            }
          });
          print("tap???????");
        },
        child:
        new Stack(
          children: <Widget>[
            //文字列表
            new AnimatedContainer(
              curve: Curves.ease,
              duration: new Duration(milliseconds: 500),
              height: item.showDetail == false ? itemHeight : item
                  .detailHeight,
              width: 320.0,
              alignment: Alignment.topRight,
              margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
              decoration: new BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.circular(5.0),
                boxShadow: const <BoxShadow>[
                  BoxShadow(
                    offset: Offset(0.0, 3.0),
                    blurRadius: 2.0,
                    spreadRadius: -1.0,
                    color: Color(0x33000000),
                  ),
                ],
              ),
              child: new Column(
                //mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    height: 20.0,
                    width: 180.0,
                    margin: EdgeInsets.fromLTRB(0.0, 10.0, 20.0, 0.0),
//                      decoration: new BoxDecoration(
//                        color: Colors.lightBlue,
//                        borderRadius: BorderRadius.circular(5.0),
//                      ),
                    child: new Text(
                        item.title, style: new TextStyle(fontSize: 15.0)),
                  ),
                  new Container(
                    height: 60.0,
                    width: 180.0,
//                      margin: EdgeInsets.fromLTRB(0.0, 5.0, 60.0, 0.0),
//                      decoration: new BoxDecoration(
//                        color: Colors.lightBlue,
//                        borderRadius: BorderRadius.circular(5.0),
//                      ),
                    child: new Text(
                        item.introduction, style: new TextStyle(fontSize: 14.0,color:Colors.grey,height: 1.2)),
                  ),
                  new Container(
                    width: 180.0,
                    child: item.showDetail == false ? null :  new Text(
                        item.content, style: new TextStyle(fontSize: 14.0,color:Colors.grey,height: 1.2)),
                  )
                ],
              ),
            ),

            //缩略图和图片列表
            new AnimatedContainer(
              curve: Curves.ease,
              height: item.showDetail == false ? itemHeight - 20 : item
                  .detailHeight - 20,
              width: item.showDetail == false ? itemHeight - 20 : 360.0,
              margin: EdgeInsets.fromLTRB(
                  item.showDetail == true ? 0.0 : 10.0, 30.0, 0.0, 0.0),
              duration: new Duration(milliseconds: 500),
              child: item.showDetail == true ? new ListView(
                reverse: true,
                padding: EdgeInsets.fromLTRB(0.0, 0.0, 230.0, 0.0),
                // padding: new EdgeInsets.symmetric(vertical: 4.0),
                scrollDirection: Axis.horizontal,
                controller: _scrollController,
                children: item.images.map<Widget>(buildListItemInner).toList(),
              ) : new Container(
                decoration: new BoxDecoration(
                  color: Colors.white,
                  borderRadius: BorderRadius.circular(5.0),
                  border: Border.all(color: Colors.white),
                  boxShadow: const <BoxShadow>[
                    BoxShadow(
                      offset: Offset(0.0, 3.0),
                      blurRadius: 2.0,
                      spreadRadius: -1.0,
                      color: Color(0x33000000),
                    ),
                  ],
                ),
                child: new Image.asset(item.url),
              ),
            ),

            //购物圆球
            new AnimatedContainer(
              curve: Curves.ease,
              duration: new Duration(milliseconds: 500),
              margin: new EdgeInsets.fromLTRB(260.0,
                  item.showDetail == false ? itemHeight - 50.0 : item
                      .detailHeight - 50.0, 0.0, 0.0),
              child: item.showDetail == false
                  ? new Opacity(opacity: 0.0)
                  : new GestureDetector(
                  onTapUp: (TapUpDetails details) {
                    setState(() {
                      item.showDetail = !item.showDetail;
                    });
                    startConfirm(details);
                  },
                  child: new Container(
                    width: 50.0,
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: Colors.pink,
                      //borderRadius: BorderRadius.circular(5.0),
                      shape: BoxShape.circle,
                    ),
                  )
              ),
            ),

          ],
        )

    );
  }

  Widget buildList(DataItem item) {
    return Container(
      key: Key(item.title),
      child: buildListItem(item),
    );
  }


  //REORDER
  void _onReorder(int oldIndex, int newIndex) {
    setState(() {
      if (newIndex > oldIndex) {
        newIndex -= 1;
      }
      final DataItem item = dataItemList.removeAt(oldIndex);
      dataItemList.insert(newIndex, item);
    });
  }


  //REFRESH
  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<Null> _handleRefresh() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));
    print("refresh");
    return null;
  }


  //ADD and REMOVE
  final GlobalKey<AnimatedListState> operateKey = GlobalKey<
      AnimatedListState>();
  OperateDataList<DataItem> operateDataList;
  DataItem newDataItem;
  DataItem selectedDataItem;


  Widget _buildWidgetItem(DataItem item, Animation<double> animation, bool selected) {

    return  Padding(
      padding: const EdgeInsets.all(2.0),
      child: SizeTransition(
          axis: Axis.vertical,
          sizeFactor: animation,
          child: Container(key: Key(item.title),
            child: new Opacity(
                opacity: selectedDataItem == item ? 0.5 : 1.0,
                child: buildListItem(item)
            ),
          )
      ),
    );
  }


  Widget buildWidgetItem(BuildContext context, int index, Animation<double> animation) {
    DataItem item = operateDataList[index];
    return _buildWidgetItem(item,animation, selectedDataItem == operateDataList[index]);
  }

  Widget buildRemovedWidgetItem(DataItem item, BuildContext context, Animation<double> animation) {
    return _buildWidgetItem(item, animation, false);
  }

  void insertNew() {
    final int index = selectedDataItem == null
        ? operateDataList.length
        : operateDataList.indexOf(selectedDataItem);
    operateDataList.insert(index, newDataItem);
  }

  void removeSelected() {
    if (selectedDataItem != null) {
      operateDataList.removeAt(operateDataList.indexOf(selectedDataItem));
      setState(() {
        selectedDataItem = null;
      });
    }
  }

  //main build
  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 3,
      child: new WillPopScope(
          child: new Stack(
            children: <Widget>[
              new Scaffold(
                backgroundColor: Colors.black,
                appBar: new AppBar(
                  automaticallyImplyLeading: false,
                  backgroundColor: Colors.black,
                  elevation: 1.0,
                  actions: selectedDataItem == null ? null : <Widget>[
                    IconButton(
                      color: Color(0xFFEB841C),
                      icon: const Icon(Icons.add_circle),
                      onPressed: insertNew,
                      tooltip: 'insert a new item',
                    ),
                    IconButton(
                      color: Color(0xFFEB841C),
                      icon: const Icon(Icons.remove_circle),
                      onPressed: removeSelected,
                      tooltip: 'remove the selected item',
                    ),
                  ],
//                  bottom: new TabBar(
//                    tabs: <Widget>[
//                      new Tab(text: "Title"),
//                      new Tab(text: "Title"),
//                      new Tab(text: "Title"),
//                    ],
//                    controller: _tabController,
//                    // indicatorColor: Colors.red,
//                    // indicatorWeight: 15.0,
//                    labelColor: Color(0xFFEB841C),
//                    labelStyle: new TextStyle(fontSize: 18.0),
//                    unselectedLabelColor: Color(0xFF999999),
//                    unselectedLabelStyle: new TextStyle(fontSize: 16.0),
//                    indicatorPadding: new EdgeInsets.only(bottom: 20.0),
//                    indicator: new BoxDecoration(
//                      color: Colors.white24,
//                      borderRadius: BorderRadius.circular(5.0),
//                    ),
//                  ),

          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(48.0),
            child: Theme(
              data: Theme.of(context).copyWith(accentColor: Colors.white),
              child: Container(
                height: 48.0,
                alignment: Alignment.center,
                child: StaticTabBarSelector(controller: _tabController,
                    tabs:new TabBar(
                      tabs: <Widget>[
                        new Container(width:68.0,child: new Tab(text: "Title"),),
                        new Container(width:68.0,child: new Tab(text: "Title"),),
                        new Container(width:68.0,child: new Tab(text: "Title"),),

                      ],
                      controller: _tabController,
                       indicatorColor: Colors.white,
                      // indicatorWeight: 15.0,
                      isScrollable:true,
                      labelColor: Colors.white,
                      labelStyle: new TextStyle(fontSize: 18.0),
                      unselectedLabelColor: Color(0xFF999999),
                      unselectedLabelStyle: new TextStyle(fontSize: 18.0),
//                      indicatorPadding: new EdgeInsets.only(bottom: 20.0),
//                      indicator: new BoxDecoration(
//                        color: Colors.white24,
//                        borderRadius: BorderRadius.circular(5.0),
//                      ),
                    ),
                    color: Color(0xFFCCCCCC),
                    selectedColor: Color(0xFF999999)),
              ),
            ),
          ),
                ),
                body: new GestureDetector(
                  onLongPressUp: () {
                    setState(() {
                      editState = true;
                     // itemHeight = 100.0;
                    });
                  },
                  child:  editState ? new Container(
                    color: Color(0xF8F8F8F8),
                  ): new TabBarView(
                    children: <Widget>[
//                  new RefreshIndicator(
//                    key: const Key("aa"),
//                    child: new Container(
//                      margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0.0),
//                      decoration: new BoxDecoration(
//                        color: Color(0xF8F8F8F8),
//                        borderRadius: BorderRadius.circular(5.0),
//                      ),
//                      child:new ListView(
//                        children: dataItemList.map<Widget>(buildList).toList(),
//                      ),
//                    ),
//                    onRefresh: _handleRefresh,
//                  ),
                      new Container(
                        margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0.0),
                        decoration: new BoxDecoration(
                          color: Color(0xF8F8F8F8),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child:new ListView(
                          children: dataItemList.map<Widget>(buildList).toList(),
                        ),
                      ),
                      new Container(
                        margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0.0),
                        decoration: new BoxDecoration(
                          color: Color(0xF8F8F8F8),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child:new ListView(
                          children: dataItemList.map<Widget>(buildList).toList(),
                        ),
                      ),
//                  new Container(
//                    margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0.0),
//                    decoration: new BoxDecoration(
//                      color: Color(0xF8F8F8F8),
//                      borderRadius: BorderRadius.circular(5.0),
//                    ),
//                    child: AnimatedList(
//                      key: operateKey,
//                      initialItemCount: operateDataList.length,
//                      itemBuilder: buildWidgetItem,
//                    ),
//                  ),
                      new Container(
                        margin: EdgeInsets.fromLTRB(2.0, 0.0, 2.0, 0.0),
                        decoration: new BoxDecoration(
                          color: Color(0xF8F8F8F8),
                          borderRadius: BorderRadius.circular(5.0),
                        ),
                        child:new ListView(
                          children: dataItemList.map<Widget>(buildList).toList(),
                        ),
                      ),
                    ],
                  ),
                ),
                floatingActionButton: show ?  new Opacity(opacity: 0.0) : ConfirmBox(
                    controller: controller,
                    doubleAnimation: animation,
                    start: start),
              ),
              new Container(
                child: editState == false? null: new Stack(
                  children: <Widget>[
                    new BackdropFilter(
                      filter: new ui.ImageFilter.blur(sigmaX: 3, sigmaY: 3),
                      child: new Container(

                        color: Colors.black.withOpacity(0.1),
                      ),
                    ),
                    new Scaffold(
                      backgroundColor: Colors.transparent,
                      body: EditableListView(
                        padding: EdgeInsets.fromLTRB(0.0, 100.0, 0.0, 0.0),

                        onReorder: _onReorder,
                        scrollDirection: Axis.vertical,
                        children: dataItemList.map<Widget>(buildList).toList(),
                      ),
                    ),
                  ],
                ),
              ),

            ],
          ),
          onWillPop: (){
            print("abc");
            if (editState) {
              setState(() {
                editState = false;
                //itemHeight = 140.0;
              });
            } else {
              Navigator.pop(context);
            }
          })
    );
  }
}
