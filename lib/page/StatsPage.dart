import 'package:flutter/material.dart';
import 'package:flutteritem/page/ContentPage.dart';
import 'package:flutteritem/page/WhiteContentPage.dart';

class StatsPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => StatsPageState();
}

class StatsPageState extends State<StatsPage>
    with SingleTickerProviderStateMixin {

  Animation<double> increaseAnimation;
  Animation<double> scaleAnimation;
  Animation<double> lineScaleAnimation;
  Animation<double> translateAnimation;
  Animation<double> goAnimation;
  AnimationController controller;

  @override
  void initState() {
    super.initState();

    controller = AnimationController(
        vsync: this, duration: Duration(milliseconds: 1500));
    final CurvedAnimation curve = new CurvedAnimation(
        parent: controller, curve: Curves.ease);
    final CurvedAnimation bounceIn = new CurvedAnimation(
        parent: controller, curve: Curves.bounceIn);
    increaseAnimation = new Tween(begin: 0.0, end: 142857.0).animate(curve)
      ..addListener(() {
        setState(() {
          print(increaseAnimation.value);
        });
      });

    scaleAnimation = new Tween(begin: 0.0, end: 1.0).animate(curve);
    lineScaleAnimation = new Tween(begin: 0.0, end: 1.0).animate(bounceIn);
    translateAnimation = new Tween(begin: 200.0, end: 0.0).animate(curve);
    goAnimation = new Tween(begin: 7.0, end: 0.0).animate(curve);
    controller.forward(from: 0.0);
  }

  void incre() {
    setState(() {
      controller.forward(from: 0.0);
    });
  }

  static FadeTransition createTransition(Animation<double> animation,
      Widget child) {
    return new FadeTransition(
        opacity: new Tween<double>(begin: 0.0, end: 1.0,).animate(animation),
        child: child);
  }


  Widget build(BuildContext context) {
    return new Scaffold(
        backgroundColor: Colors.black,
        appBar: new AppBar(
          automaticallyImplyLeading: false,
          backgroundColor: Colors.black,
          elevation: 1.0,
          actions: <Widget>[
          ],
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(15.0),
            child: Theme(
              data: Theme.of(context).copyWith(accentColor: Colors.white),
              child: Container(
                height: 15.0,
                alignment: Alignment.center,
              ),
            ),
          ),
        ),
        body: new Container(
          decoration: new BoxDecoration(
            color: Colors.grey[200],
            borderRadius: BorderRadius.circular(5.0),
          ),
          child: new Column(
            children: <Widget>[
              new Container(
                height: 320,
                child: new Stack(
                  alignment: AlignmentDirectional.center,
                  children: <Widget>[
                    new Container(
                      width: 260.0 * scaleAnimation.value,
                      height: 260.0 * scaleAnimation.value,
                      decoration: new BoxDecoration(
                        shape: BoxShape.circle,
                        color: Colors.white,
                      ),
                    ),
                    new Container(

                      width: 200.0 * lineScaleAnimation.value,
                      height: 200.0 * lineScaleAnimation.value,
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.white,
                          border: new Border.all(color: Colors.grey[200])
                      ),
                    ),
                    new Container(
                      width: 220.0 * lineScaleAnimation.value,
                      height: 220.0 * lineScaleAnimation.value,
                      decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.transparent,
                          image: new DecorationImage(
                            image: new ExactAssetImage("images/Path.png"),)
                      ),
                    ),
                    new Container(
                        width: 140.0 * scaleAnimation.value,
                        height: 140.0 * scaleAnimation.value,
                        decoration: new BoxDecoration(
                          shape: BoxShape.circle,
                          color: Colors.grey[200],
                        )
                    ),

                    new Center(
                      child: new Text(
                        increaseAnimation.value.toInt().toString(),
                        style: new TextStyle(fontSize: 25.0),),
                    )
                  ],
                ),
              ),

              new Container(
                  height: 70.0,
                  child: new Row(
                    mainAxisAlignment: MainAxisAlignment.spaceAround,
                    children: <Widget>[
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text("Total Number",
                            style: new TextStyle(color: Colors.grey),),
                          new Text("1314", style: new TextStyle(
                              color: Colors.black, fontSize: 25.0),),
                          new Text(
                            "piece", style: new TextStyle(color: Colors.grey),),
                        ],
                      ),
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text("Number of stoe",
                            style: new TextStyle(color: Colors.grey),),
                          new Text("8", style: new TextStyle(
                              color: Colors.black, fontSize: 25.0),),
                          new Text(
                            "piece", style: new TextStyle(color: Colors.grey),),
                        ],
                      ),
                      new Column(
                        mainAxisAlignment: MainAxisAlignment.spaceAround,
                        children: <Widget>[
                          new Text("Total Time",
                            style: new TextStyle(color: Colors.grey),),
                          new Text("45:54", style: new TextStyle(
                              color: Colors.black, fontSize: 25.0),),
                          new Text(
                            "piece", style: new TextStyle(color: Colors.grey),),
                        ],
                      ),
                    ],
                  )
              ),
              new GestureDetector(
                  onTap: () {
                    Navigator.of(context).push(
                        new PageRouteBuilder(
                            pageBuilder: (BuildContext context,
                                Animation<double> animation,
                                Animation secAnimation) {
                              return new ContentPage(title: 'animation');
                            },
                            transitionDuration: const Duration(
                                milliseconds: 500),
                            transitionsBuilder: (BuildContext context,
                                Animation<double> animation,
                                Animation secAnimation, Widget child) {
                              return createTransition(animation, child);
                            })
                    );
                  },
                  child: new Transform.translate(
                      offset: Offset(0.0, translateAnimation.value),
                      child: new Hero(
                        tag: 'card',
                        child: new Container(
                          width: 320.0,
                          margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
                          height: 180.0,
                          decoration: new BoxDecoration(
                            color: Colors.black,
                            borderRadius: BorderRadius.circular(5.0),
                            boxShadow: const <BoxShadow>[
                              BoxShadow(
                                offset: Offset(0.0, 3.0),
                                blurRadius: 2.0,
                                spreadRadius: -1.0,
                                color: Color(0x33000000),
                              ),
                            ],
                          ),
                        ),
                      )
                  )
              ),

            ],
          ),
        )
    );
  }
}