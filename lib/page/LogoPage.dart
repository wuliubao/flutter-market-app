import 'package:flutter/material.dart';
import 'dart:math';

import 'package:flutteritem/page/StatsPage.dart';

class LogoPage extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => LogoPageState();
}

class LogoPageState extends State<LogoPage> with SingleTickerProviderStateMixin {

  Animation<double> translateAnimation;
  AnimationController controller;
  @override
  void initState(){
    super.initState();
    controller = AnimationController(vsync: this, duration: Duration(milliseconds: 500));
    final CurvedAnimation curve = new CurvedAnimation(parent: controller, curve: Curves.ease);
    translateAnimation = new Tween(begin:400.0, end:0.0).animate(curve)
      ..addListener(() {
        setState(() {
          print(translateAnimation.value);
        });
      });
    controller.forward(from:0.0);

  }
  static SlideTransition createTransition(Animation<double> animation, Widget child){
    return new SlideTransition(
        position: new Tween<Offset>(begin: const Offset(0.0,1.0), end: const Offset(0.0,0.0),).animate(animation),child:child);
  }

  Widget build(BuildContext context){
    return new Scaffold(
        body: new GestureDetector(
            onTap:  (){
              setState(() {
                controller.reverse();
              });
              Navigator.of(context).push<String>(
                  new PageRouteBuilder(
                      pageBuilder: (BuildContext context, Animation<double> animation, Animation secAnimation){
                        return new StatsPage();
                      },
                      transitionDuration: const Duration(milliseconds: 500),
                      transitionsBuilder: (BuildContext context, Animation<double> animation, Animation secAnimation, Widget child){
                        return createTransition(animation,child);
                      })
              ).then( (String string){
                print("whatfuck?");
                setState(() {
                  controller.forward(from:0.0);
                });
              });
            },
            child:new Container(
              alignment: Alignment.center,
              color: Colors.black,
              child:  new Transform(
                  transform: new Matrix4.rotationZ(pi/2),
                  alignment: FractionalOffset.center,
                  child: new Transform.translate(
                    offset: Offset(translateAnimation.value, 0.0),
                    child: new Text('HUAWEI',
                      style: new TextStyle(
                          color: Colors.white,
                          fontSize: 80.0
                      ),
                    ),
                  ),
              ),
            )
        )
    );
  }
}