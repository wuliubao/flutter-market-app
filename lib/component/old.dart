import 'package:flutter/material.dart';
import 'package:flutter/animation.dart';
import 'package:flutter_swiper/flutter_swiper.dart';
import 'confirm.dart';
import 'StaticTabBarSelector.dart';


void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Animation Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new MyHomePage(title: 'animation'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => new _MyHomePageState();
}

class _ListItem {
  _ListItem(this.value, this.checkState);

  final String value;

  double height = 100.0;
  bool checkState;
}

class _MyHomePageState extends State<MyHomePage>
    with TickerProviderStateMixin {

  bool show = true;

  void startDrag() {
    setState(() {
      //show = false;
    });
  }

  void startConfirm(TapUpDetails details) {
    print("touch start?");
//    start = (context.findRenderObject() as RenderBox)
//        .globalToLocal(details.globalPosition);

    setState(() {
      start = details.globalPosition;
      show = false;
      controller = AnimationController(
          vsync: this, duration: Duration(milliseconds: 2000));
      final CurvedAnimation curve = new CurvedAnimation(
          parent: controller, curve: Curves.linear);
      animation = new Tween(begin: 0.0, end: 2.0).animate(curve)
        ..addListener(() {
          setState(() {
            // the state that has changed here is the animation object’s value
          });
        })
        ..addStatusListener((status) {
          if (status == AnimationStatus.forward) {
            print("动画开始");
          } else if (status == AnimationStatus.completed) {
            print("动画结束");
            show = true;
            controller.stop();
          }
          else if (status == AnimationStatus.dismissed) {
            print("动画消失");
            //   show = true;
            // controller.forward();
          }
        });
      controller.forward(from: 0.0);
    });
  }

  ScrollController _scrollController = new ScrollController();

  Animation<double> animation;
  AnimationController controller;
  Offset start;

  @override
  void initState() {
    _scrollController.addListener(() {
      print("scroll?");
    });
//    for (ScrollPosition position in List <ScrollPosition>.from(_scrollController.positions))
//      position.jumpTo(100.0);
    // _scrollController.jumpTo(10.0);
    //  _scrollController.jumpTo(100.0);
    super.initState();
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }

  Widget buildList(_ListItem item) {
    const Widget secondary = Text(
      'Even more additional list item information appears on line three.',
    );
    Widget list;
    list = Container(
      key: Key(item.value),
      //height: 100.0,
      //`\\width: 100.0,
      child: buildItem(item),
    );

    return list;
  }

  Widget buildItem(_ListItem item) {
    return new GestureDetector(
        onTap: () {
          setState(() {
            item.height = item.height == 100 ? 200.0 : 100.0;
          });
          print("tap???????");
        },
        child:
        new Stack(
          children: <Widget>[
            new AnimatedContainer(
              //curve: Curves.bounceOut,
              curve: Curves.ease,
              //duration: new Duration(seconds: 1),
              duration: new Duration(milliseconds: 500),
              height: item.height,
              width: 320.0,
              alignment: Alignment.topRight,
              margin: EdgeInsets.fromLTRB(20.0, 20.0, 20.0, 0.0),
              decoration: new BoxDecoration(
                color: Colors.white70,
                borderRadius: BorderRadius.circular(5.0),
              ),
              child: new Column(
                mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Container(
                    height: 20.0,
                    width: 180.0,
                    margin: EdgeInsets.fromLTRB(0.0, 10.0, 50.0, 0.0),
                    decoration: new BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  new Container(
                    height: 15.0,
                    width: 100.0,
                    margin: EdgeInsets.fromLTRB(0.0, 5.0, 50.0, 0.0),
                    decoration: new BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
                  new Container(
                    height: 15.0,
                    width: 100.0,
                    margin: EdgeInsets.fromLTRB(0.0, 5.0, 50.0, 0.0),
                    decoration: new BoxDecoration(
                      color: Colors.lightBlue,
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                  ),
//                  new RaisedButton(
//                    child: new Text('GO'),
//                    onPressed: startDrag,
//                  ),
                ],
              ),
            ),
            new AnimatedContainer(
              curve: Curves.ease,
              height: -20 + item.height,
              width: item.height == 200.0 ? 400.0 : 80.0,
              margin: EdgeInsets.fromLTRB(
                  item.height == 200.0 ? 0.0 : 10.0, 30.0, 0.0, 0.0),
              duration: new Duration(milliseconds: 500),
              child: item.height == 200.0 ? new ListView(
                  padding: EdgeInsets.fromLTRB(0.0, 0.0, 280.0, 0.0),
                  // padding: new EdgeInsets.symmetric(vertical: 4.0),
                  scrollDirection: Axis.horizontal,
                  controller: _scrollController,
                  children: [
                    new Image.asset("images/placeholder.png"),
                    new Image.asset("images/placeholder.png"),
                    new Image.asset("images/placeholder.png"),
                    new Image.asset("images/placeholder.png"),
                    new Image.asset("images/placeholder.png"),
                    new Image.asset("images/placeholder.png"),
                  ]
              ) : new Image.asset("images/placeholder.png"),
            ),
            new AnimatedContainer(
              curve: Curves.ease,
              duration: new Duration(milliseconds: 500),
              margin: new EdgeInsets.fromLTRB(
                  260.0, -50.0 + item.height, 0.0, 0.0),
              child: item.height == 100 ? new Opacity(opacity: 0.0) : new GestureDetector(
                  onTapUp: (TapUpDetails details) {
                    setState(() {
                      item.height = item.height == 100 ? 200.0 : 100.0;
                    });
                    startConfirm(details);
                  },
                  child: new Container(
                    width: 50.0,
                    height: 50.0,
                    decoration: BoxDecoration(
                      color: Colors.pink,
                      //borderRadius: BorderRadius.circular(5.0),
                      shape: BoxShape.circle,
                    ),
                  )
              ),
            ),

          ],
        )

    );
  }

  TabController _tabController;

  final List<_ListItem> _items = <String>[
    'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N',
  ].map<_ListItem>((String item) => _ListItem(item, false)).toList();

  void _onReorder(int oldIndex, int newIndex) {
    setState(() {
      if (newIndex > oldIndex) {
        newIndex -= 1;
      }
      final _ListItem item = _items.removeAt(oldIndex);
      _items.insert(newIndex, item);
    });
  }

  var refreshKey = GlobalKey<RefreshIndicatorState>();

  Future<Null> _handleRefresh() async {
    refreshKey.currentState?.show(atTop: false);
    await Future.delayed(Duration(seconds: 2));
    print("refresh");
    return null;
  }

  @override
  Widget build(BuildContext context) {
    return new DefaultTabController(
      length: 3,
      child: new Scaffold(
        appBar: new AppBar(
          title: new Text('animation'),
//          bottom: new TabBar(
//            tabs: <Widget>[
//              new Tab(icon: new Icon(Icons.view_stream),),
//              new Tab(icon: new Icon(Icons.view_stream),),
//              new Tab(icon: new Icon(Icons.view_stream),),
//            ],
//            controller: _tabController,
//            indicatorColor: Colors.red,
//            indicatorWeight: 15.0,
//            //  indicatorPadding: new EdgeInsets.only(bottom: 20.0),
////            indicator: new BoxDecoration(
////              color: Colors.white24,
////              borderRadius: BorderRadius.circular(5.0),
////            ),
//          ),
          bottom: PreferredSize(
            preferredSize: const Size.fromHeight(48.0),
            child: Theme(
              data: Theme.of(context).copyWith(accentColor: Colors.white),
              child: Container(
                height: 48.0,
                alignment: Alignment.center,
                child: StaticTabBarSelector(controller: _tabController,
                    color: Colors.white70,
                    selectedColor: Colors.white),
              ),
            ),
          ),
        ),
        body: new TabBarView(
          children: <Widget>[

            new RefreshIndicator(
              child: new Container(
                color: Colors.lightBlue,
                child: new ListView(
                  children: _items.map<Widget>(buildList).toList(),
                ),
              ),
              onRefresh: _handleRefresh,
            ),

            new Container(
              color: Colors.lightBlue,
              child: new ListView(
                children: _items.map<Widget>(buildList).toList(),
              ),
            ),
            new Container(
              color: Colors.lightBlue,
              child: ReorderableListView(
//                header: Padding(
//                    padding: const EdgeInsets.all(8.0),
//                    child: Text('Header of the list', style: Theme.of(context).textTheme.headline)),
                onReorder: _onReorder,
                scrollDirection: Axis.vertical,
                padding: const EdgeInsets.symmetric(vertical: 8.0),
                children: _items.map<Widget>(buildList).toList(),
              ),
            ),
          ],
        ),
        floatingActionButton: show ? new Opacity(opacity: 0.0) : ConfirmBox(
            controller: controller,
            doubleAnimation: animation,
            start: start),
      ),
    );
  }
}


