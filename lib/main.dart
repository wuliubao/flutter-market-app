import 'package:flutter/material.dart';

import 'package:flutteritem/page/LogoPage.dart';
import 'package:flutteritem/page/StatsPage.dart';
import 'package:flutteritem/page/ContentPage.dart';

void main() => runApp(new MyApp());

class MyApp extends StatelessWidget {

  @override
  Widget build(BuildContext context) {
    return new MaterialApp(
      title: 'Animation Demo',
      theme: new ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: new LogoPage(),
      routes: <String, WidgetBuilder>{
        'LogoPage':(BuildContext context) => new LogoPage(),
        'statsPage':(BuildContext context) => new StatsPage(),
        'ContentPage':(BuildContext context) => new ContentPage(title: 'animation'),
      },
    );
  }

}







